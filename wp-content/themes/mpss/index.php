<?php get_header(); ?>


<div class="banner homeBanner">
    	<h2>Optimizing the Quality, Safety <br />
        and Dignity of the <span>Aging Experience</span></h2>
    </div>
    
    <div id="content">
    	<div class="greenbox home">
        	<p>&nbsp;</p>
        </div>
        <div class="main-cont">
        	<div class="cont-top">
                <div class="cont-t-left home">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mainPage.jpg">
                </div>
                <div class="cont-t-right home">
                    <ul class="mainCont">
                        <li>Information for Life Kit &nbsp;<a href="life.htm">Learn more &gt;&gt;</a></li>
                        <li>The Meltzer Advantage &nbsp;<a href="advantage.htm">Learn more &gt;&gt;</a></li>
                        <li>Senior Passport &nbsp;<a href="concierge.htm">Learn more &gt;&gt;</a></li>
                    </ul>
                </div>
            </div>
            <div class="cont-bottom">
            	<div class="cont-b-left">
                	<p>&nbsp;</p>
                </div>
                <div class="cont-b-mid">
                	<p><a href="#">Back to top</a></p>
                </div>
                <div class="cont-b-right">
                	<p>Society of Certiﬁed Senior Advisors <br /><a href="www.csa.us" target="_blank">www.csa.us</a></p>
                </div>
            </div>
        </div>
   	</div>

<?php get_footer();	?>